<?php

session_start();
include_once(__DIR__.'/database/Connection.php');
$_connection = new Connection();

$status = 'success';
$message = 'Success';
$result = array();

$action = "";
if($_POST['action']){
    $action = $_POST['action'];
}


if($action == 'test'){
    $name="";
    if($_POST['name']){
        $name = $_POST['name'];
    }

    $test="";
    if($_POST['test']){
        $test = $_POST['test'];
    }

    if($name!='' && $test!=''){
        $allQuestions = array();
        $allTest = $_connection->General("SELECT * FROM tests WHERE id = '$test' ORDER BY id");

        if(count($allTest)>0){
            $userId = $_connection->Insert('users',array('name'=>$name));
            $questions = $_connection->General("SELECT * FROM questions  WHERE test = '$test' ORDER BY id");

            foreach($questions as $question){
                $questionId=$question['id'];
                $answers = $_connection->General("SELECT * FROM answers  WHERE question = '$questionId' ORDER BY id");
                $allQuestions[$questionId]=array(
                    'question' => $question,
                    'answers' => $answers,
                );
            }

            $result = array(
                'user' => $userId,
                'test' => $allTest[0],
                'questions' => $allQuestions
            );

        }else{
            $status = 'error';
            $message = 'Error with Test';
        }


    }else{
        $status = 'error';
        $message = 'Complete Form';
    }



}else if($action == 'questions'){
    $user="";
    if($_POST['user']){
        $user = $_POST['user'];
    }

    $test="";
    if($_POST['test']){
        $test = $_POST['test'];
    }

    $response=array();
    if($_POST['response'] && is_array($_POST['response'])){
        $response = $_POST['response'];
    }


    if($user!='' && $test!='' && count($response)>0){

        //GET USER
        $query = "SELECT * FROM users WHERE id = '$user' ORDER BY id";
        $users = $_connection->General($query);

        $userName = "";
        foreach($users as $data){
            $userName = $data['name'];
        }


        //GET ALL ANSWERS CORRECTLY PER TEST
        $query = "SELECT answers.* FROM answers LEFT JOIN questions ON questions.id = answers.question WHERE questions.test = '$test' && answers.result = 'true'  ORDER BY questions.id";
        $answers = $_connection->General($query);

        $allAnswer = array();
        foreach($answers as $answer){
            $answerId = $answer['id'];
            $questionId = $answer['question'];

            if(!isset($allAnswer[$questionId])){
                $allAnswer[$questionId] = array();
            }

            $allAnswer[$questionId][]=$answerId;
        }


        //EVAL RESULT WITH LIST
        $totalAnswer = 0;
        $correctlyAnswer = 0;
        $correctlyQuestions = 0;
        foreach($allAnswer as $question => $answers){
            $correctlyAnswerPerQuestion = 0;
            $totalAnswer=$totalAnswer+count($answers);

            if(isset($response[$question])){
                foreach($answers as $answer){
                    if(in_array($answer, $response[$question])){
                        $correctlyAnswerPerQuestion++;
                        $correctlyAnswer++;
                    }
                }

            }else{

            }

            if(count($response[$question])>count($answers)){
                $correctlyAnswerPerQuestion=$correctlyAnswerPerQuestion-(count($response[$question])-count($answers));
            }

            if($correctlyAnswerPerQuestion == count($answers)){
                $correctlyQuestions++;
            }
        }

        //SET MESSAGE
        $message = '<h2>'.$userName.' Thanks for participating</h2>';
        $message .= '<h4>You have obtained '.$correctlyQuestions.' out of '.count($allAnswer).' correct questions</h4>';
        $message .= '<h4>You have obtained '.$correctlyAnswer.' out of '.$totalAnswer.' correct answers</h4>';

        //SAVE DATA
        $userId = $_connection->Insert('users_answer',array(
            'user'=>$user,
            'test'=>$test,
            'answer'=>json_encode($response, JSON_FORCE_OBJECT),
            'result'=>$message,
            'user'=>$user,
        ));
    }
}


echo json_encode(array(
    'action' => $action,
    'status' => $status,
    'message' => $message,
    'result' => $result,
),JSON_FORCE_OBJECT);

?>