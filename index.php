<?php

session_start();
include_once(__DIR__.'/database/Connection.php');
$_connection = new Connection();

$users = $_connection->General('SELECT * FROM users ORDER BY id DESC LIMIT 0, 1');
$tests = $_connection->General('SELECT * FROM tests ORDER BY id');

$userId = 0;
if(count($users)>0){
    $userId = $users[0]['id'];
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TEST</title>

    <style>
        body, html{
            font-family: "Arial";
            background: #eee;
        }

        .contain{
            width:100%;
            max-width:700px;
            margin: 0 auto;
        }

        .content{
            position: relative;
            margin: 50px;
            padding: 20px;
            background: #fff;
            border-radius: 15px;
            box-shadow: 0 0 10px #000;
            overflow: hidden;
        }

        .content form{
            position: relative;
            text-align: center;
            max-width: 300px;
            margin:0 auto;
        }

        .content form select,
        .content form input,
        .content form button{
            position: relative;
            margin: 10px 0;
            padding: 10px;
            width: 100%;
        }

        .content form input{
            width: calc(100% - 20px);
        }

        /*SECTION*/
        .section{
            position: absolute;
            opacity: 0;
            left:calc(100% + 1000px);
            transition: all .5s;
        }

        .section.show{
            position: relative;
            opacity: 1;
            left:0px;
        }

        .section.prev{
            left:-1000px;
        }


        /*QUESTIONS*/
        #allQuestions{

        }

        #allQuestions .question{
            display:none;
        }

        #allQuestions .question.show{
            display:block;
        }

        /*ANSWER*/
        .answers {
            display: flex;
            flex-flow: wrap;
        }

        .answer {
            position: relative;
            width: calc(50% - 20px);
            margin: 10px;
            box-shadow: 3px 3px 5px #AAA;
            cursor: pointer;
        }

        .answerBack{
            position: absolute;
            width: 100%;
            height: 100%;
            top:0;
            left:0;
            background: #FFF;
        }

        .answer input[type="checkbox"]{
            display: none;
        }

        .answer input[type="checkbox"]:checked + .answerBack {
            color: #FFF;
            background: #35dbe7;
            font-weight: bold;
        }

        .answerTitle {
            position: relative;
            padding: 10px;
            color: #555;
        }

        .answer input[type="checkbox"]:checked  ~ .answerTitle {
            color: #FFF;
            font-weight: bold;
        }

        /*progressBar*/
        #progressBar{
            height: 20px;
            display:flex;
            align-items: center;
            border: 1px solid #CCC;
            border-radius: 5px;
        }

        #progressBar div{
            height: 100%;
            width: 100%;
            background: #009900;
        }

        #progressBar div.next{
            background: #FFF;
        }


        @media (max-width: 480px) {
            .answer {
                width: calc(100% - 20px);
            }
        }
    </style>


    <script type="text/javascript">

        async function formSubmit(form){
            var url = 'api.php';
            var data = new FormData(form);
            data.append('action','test');

            if(data.get('name') == ''){
                alert('Insert Name');
                return;

            }else if(data.get('test') == ''){
                alert('Choose Test');
                return;
            }

            var result = await setData(url, data);

            if(result.status == 'error'){
                alert('Error: '+result.message);

            }else if(result.status == 'success'){
                //alert('Success: '+result.message);
                changeSection('questions');

                var data = result.result;

                document.querySelector('#questions input[name="user"]').value = data.user;
                document.querySelector('#questions input[name="test"]').value = data.test.id;
                document.querySelector('#titleTest').innerHTML = data.test.title;
                document.querySelector('#contentTest').innerHTML = data.test.content;
                var content = document.querySelector('#allQuestions');
                var porgressBar =  document.querySelector('#progressBar');

                var html = "";
                var htmlBar = "";
                content.innerHTML = html;
                porgressBar.innerHTML = htmlBar;
                var questions = data.questions;
                var iQuestion = 0;

                for(var question in questions){
                    iQuestion = iQuestion+1;

                    var showQuestion = "";
                    if(iQuestion==1){
                        showQuestion = "show";
                    }

                    html += '<div data-question="'+iQuestion+'" class="question '+showQuestion+'">';
                    html += '<h3 class="titleQuestion">'+questions[question].question.title+'</h3>';
                    html += '<h5 class="contentQuestion">'+questions[question].question.content+'</h5>';
                    html += '<div class="answers">';

                    var questionId = questions[question].question.id;
                    var answers = questions[question].answers;
                    for(var answer in answers){
                        var answerId = answers[answer].id;
                        html += '<label class="answer" for="answer'+answerId+'">';
                        html += '<input type="checkbox"  id="answer'+answerId+'" name="response['+questionId+'][]" value="'+answerId+'"/>';
                        html += '<div class="answerBack"></div>';
                        html += '<div class="answerTitle">'+answers[answer].title+'</div>';
                        html += '</label>';
                    }

                    html += '</div>';
                    html += '</div>';

                    htmlBar+= '<div class="next"></div>';
                }

                content.innerHTML = html;
                porgressBar.innerHTML = htmlBar;


            }else{
                alert('Other Error');
            }
        }


        async function formQuestions(form){
            var url = 'api.php';
            var data = new FormData(form);
            data.append('action','questions');

            //validate response
            var iQuestion = 0;
            var questions = document.querySelectorAll("#allQuestions .question");
            for (iQuestion; iQuestion < questions.length; iQuestion++) {
                var options = questions[iQuestion].querySelectorAll('input[type="checkbox"]:checked');
                console.log('option: '+options.length);

                if(options.length == 0){
                    changeQuestion(questions[iQuestion], iQuestion);
                    break;
                }
            };


            console.log('questions.length: '+questions.length+' iQuestion:'+iQuestion);
            if(questions.length > iQuestion){
                return;
            }

            var result = await setData(url, data);
            console.log(result);

            if(result.status == 'error'){
                alert('error');

            }else if(result.status == 'success'){
                document.querySelector('#thankMessage').innerHTML = result.message;
                changeSection('thanks');
                //alert('success');

            }else{
                alert('Other Error');
            }
        }


        async function setData(url, data){
            var result = await fetch(url, {
                method : "POST",
                body: data,
            }).then((response) => {
                console.log(response);
                return response.text();

            }).then((data) => {
                    console.log(data);
                    return JSON.parse(data);
            });

            return result;
        }

        function changeSection(section){
            //var elements = document.querySelector(".section");
            var elements = document.querySelectorAll(".section");
            //var elements = document.getElementsByClassName("section");
            //var element = document.getElementById("section");

            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove("show");
                elements[i].classList.add("prev");
            };

            //SHOW
            document.querySelector("#"+section).classList.add("show");
            document.querySelector("#"+section).classList.remove("prev");

            var newElements = document.querySelectorAll("#"+section+'.section ~ .section');
            for (var i = 0; i < newElements.length; i++) {
                newElements[i].classList.remove("prev");
            };

        }

        function changeQuestion(question, iQuestion){
            var questions = document.querySelectorAll("#allQuestions .question");
            for (var i = 0; i < questions.length; i++) {
                questions[i].classList.remove("show");
            };

            //SHOW
            question.classList.add("show");

           //progressBar
            var iProgressBar=0;
            var progressBar = document.querySelectorAll('#progressBar div');
            for (iProgressBar; iProgressBar < progressBar.length; iProgressBar++) {
                console.log('iProgressBar: '+iProgressBar+' iQuestion:'+iQuestion);
                if(iProgressBar >= iQuestion){
                    progressBar[iProgressBar].classList.add("next");
                }else{
                    progressBar[iProgressBar].classList.remove("next");
                }
            };
        }

    </script>
</head>

<body>

<div class="contain">
    <div class="content">
        <div id="tests" class="section show">
            <form onsubmit="formSubmit(this); return false;">
                <h1>Technical Task</h1>
                <div>
                    <input type="text" name="name" value="" placeholder="Enter Your Name"/>
                </div>
                <div>
                    <select name="test">
                        <option value="">Choose test</option>
                        <?php
                        foreach($tests as $test){
                            echo '<option value="'.$test['id'].'">'.$test['title'].'</option>';
                        }
                        ?>
                    </select>
                </div>


                <button type="submit">
                    Start
                </button>
            </form>
        </div>

        <div id="questions" class="section">
            <form onsubmit="formQuestions(this); return false;">
                <input type="hidden" name="user" value=""/>
                <input type="hidden" name="test" value=""/>
                <h1>Questions</h1>
                <h3 id="titleTest" class="tite">Title</h3>
                <h5 id="contentTest" class="description">Description</h5>

                <div id="allQuestions">
                </div>

                <div id="progressBar">
                </div>

                <button type="submit">
                    response
                </button>

            </form>
        </div>



        <div id="thanks" class="section">
            <div id="thankMessage"></div>
        </div>

    </div>
</div>
</body>
</html>
