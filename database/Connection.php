<?php

class Connection{
	var $args;

	function __construct($args=array()){
		
		$this->args = $args;
		$this->hostname = "localhost";
		$this->database = "printful";
		$this->username = "root";
		$this->password = "";
        $this->connection = null;
        $this->result = null;
	}  
	
	function Connection($query='', $close=true){
		if($query!=''){
            $this->connection = mysqli_connect($this->hostname, $this->username, $this->password, $this->database);
			if (mysqli_connect_errno()) {
				printf("Falló la conexión: %s\n", mysqli_connect_error());
				exit();
			}
			
			//UTF-8 Encoding
			//$connection->character_set_name();
			//$connection->set_charset("utf8");
			mysqli_query($this->connection, "SET NAMES 'utf8'");
			mysqli_query($this->connection, "SET CHARACTER SET 'utf8'");

            $this->result = mysqli_query($this->connection, $query);
			if (!$this->result) {
				echo 'MySQL Error: '.mysqli_error($this->connection);
				exit;
			}
		
			if($close){
                mysqli_free_result($this->result);
                mysqli_close($this->connection);
				
			}else if(!is_bool($this->result)){
                return;
			}
		}
	}
	
	function General($query=''){
		$return=array(); 
		
		if($query!=''){
			$this->Connection($query,false);
			$row = mysqli_fetch_assoc($this->result);
			$count = mysqli_num_rows($this->result);
			if($count>0){
				do {
					$return[]=$row;
				}while($row = mysqli_fetch_assoc($this->result));
			}
			
			if(!is_bool($this->result)){
				mysqli_free_result($this->result);
				mysqli_close($this->connection);
			}
		}
		return $return;
	}
   
	function Fields($args=array()){
		$args_field=array();
		$args_value=array();
		$args_both=array();
		foreach($args as $field=>$value){
			if($value!=''){
				$args_field[]=$field;
				$args_value[]="'$value'";
				$args_both[]="$field='$value'";
			}
		}
		
		$fields=array();
		$fields['field']=$args_field;
		$fields['value']=$args_value;
		$fields['both']=$args_both;		
		return $fields;	
	} 
   
	function Insert($tabla='',$args=array()){
		if($tabla!='' && count($args)>0){
			$fields=$this->Fields($args);
			$group_field=implode(',',$fields['field']);
			$group_value=implode(',',$fields['value']);
			$insert = "INSERT INTO $tabla ($group_field) VALUES ($group_value);";
			$this->Connection($insert, false);

			$ID = $this->connection->insert_id;

            if(!is_bool($this->result)){
                mysqli_free_result($this->result);
                mysqli_close($this->connection);
            }

            return $ID;
		}
	} 
   
	function Update($tabla='',$args=array(),$where=''){
		if($tabla!='' && count($args)>0){
			$fields=$this->Fields($args);
			$group_both=implode(',',$fields['both']);
			$update = "UPDATE $tabla SET $group_both WHERE $where;";
			$this->Connection($update);		
		}
	} 
   
	function Delete($tabla='',$where=''){
		if($tabla!='' && $where!=''){
			$delete = "DELETE FROM $tabla WHERE $where;";
			$this->Connection($delete);		
		}
	} 
}
?>